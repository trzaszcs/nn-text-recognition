$(document).ready(function(){
	Main.onLoad();
});

var Settings={
	canvasWidth:40,
	canvasHeight:40,
	lettersCount:8,
	canvasProbeSize:5,
	alphabet:'ABCDEFGHIJKLMNOPRSTUVWZ'.split('')
};

var ActivationUtils=(function(){
	return {
		calculate:function(value){
			return (1/(1+Math.exp(-value)));
		}
	};

})();

 var NeuronUtils=(function(){

 	function multiplyWeights(neuron,input){
 		var weights=neuron.weights,sum=0;
 		for(var c=0;c<weights.length;c++){
			sum+=weights[c]*input[c];
 		}

 		return sum;
 	}

 	function calculateActivation(sum){
		return ActivationUtils.calculate(sum);
 	}

 	return {
 		calculateActivation:function(neuron,input){
 			var sum=multiplyWeights(neuron,input);
 			var activation=calculateActivation(sum);

			neuron.activation=activation;
			neuron.output=sum;

// 			console.log("sum: "+neuron.sum+" activation: "+activation);

 			return neuron.activation;
 		}
 	};

 })();

 var NeuronFactory=(function(settings){

	function generateRandom(){
		var randomnumber=Math.floor(Math.random()*50)/100;

		// generate sign

		var randForSign=Math.floor(Math.random()*10);

		if(randForSign % 2 == 0){
			return - randomnumber;
		}else{
			return randomnumber;
		}
	}

  	function generateWeights(size){
  		var weights=[];

  		for(var c=0;c<size;c++){
  			weights[c]=generateRandom();
  		}

  		return weights;
  	}

 	return {
 		create:function(name,nrOfInputs){
 			return {
 				"name":name,
 				"weights":generateWeights(nrOfInputs),
 				"activation":0,
 				"output":0
 			};
 		}
 	};


 })(Settings);

var Binary=(function(){

	function isPolygonDrawed(ctx,startX,startY,length){

		for(var x=startX;x<startX+length;x++){
			for(var y=startY;y<startY+length;y++){
				var data=ctx.getImageData(x,y,1,1).data;
				if(data[0]!==0 || data[1]!==0 || data[2]!==0){
					return true;
				}
			}
		}

		return false;
	}

	function fillPolygon(ctx,startX,startY,length){
		ctx.fillRect(startX,startY,length,length);
	}

	function getSelectedPolygons(canvas,probeSize){
		var ctx=canvas.getContext('2d');
		var width=canvas.width;
		var height=canvas.height;
		var polygons=[];

                for(var x=0;x<width;x=x+probeSize){
                	for(var y=0;y<height;y=y+probeSize){
                        	if(isPolygonDrawed(ctx,x,y,probeSize)){
                                	polygons.push([x,y]);
                               	}

			}
                }
		return polygons;
	}

	function serializeCanvas(canvas,probeSize){
		var ctx=canvas.getContext('2d'),width=canvas.width,height=canvas.height,matrix=[],c=0;


		for(var y=0;y<height;y=y+probeSize){
			for(var x=0;x<width;x=x+probeSize){
				matrix[c]=(isPolygonDrawed(ctx,x,y,probeSize))?1:0;
				c++;
			}
		}
		return matrix;
	}

	return {
		binarizeCanvas:function(canvas,probeSize){

			var selectedPolygons=getSelectedPolygons(canvas,probeSize);
			var ctx=canvas.getContext('2d');

			for(var c=0;c<selectedPolygons.length;c++){
				var selectedPolygon=selectedPolygons[c];
				fillPolygon(ctx,selectedPolygon[0],selectedPolygon[1],probeSize);
			}

		},

		getPolygons:function(canvas,probeSize){
			return getSelectedPolygons(canvas,probeSize);
		},

		getMatrix:function(canvas,probeSize){
			return serializeCanvas(canvas,probeSize);
		}

	};

})();

var Canvas=(function(canvas){
	function relMouseCoords(event){
		var mouseX,mouseY;
		if ( event.offsetX == null ) { // Firefox
			mouseX = event.layerX;
			mouseY = event.layerY;
		} else {                       // Other browsers
			mouseX = event.offsetX;
			mouseY = event.offsetY;
		}
		return [mouseX,mouseY];
	}

	return {
			getMouseCoordinates:function(e){
				return relMouseCoords(e);
			}

		};
})();


var Main=(function(settings){

	var width=settings.canvasWidth,height=settings.canvasHeight,lettersCount=settings.lettersCount;

	function draw(ctx,x,y){
		ctx.fillStyle='#FF9000';
		ctx.fillRect(x,y,2,2);
	}

	function handleCanvasEvents($canvas){
                var ctx=$canvas[0].getContext('2d');
		var isDown=false;

		$canvas.mousedown(function(e){
			isDown=true;
			var coords=Canvas.getMouseCoordinates(e);
			draw(ctx,coords[0],coords[1]);
		});
		$canvas.mouseup(function(){
			isDown=false;
		});

               	$canvas.mousemove(function(e){
			if(isDown){
                       		var coords=Canvas.getMouseCoordinates(e);
				draw(ctx,coords[0],coords[1]);
                      	}
               	});

	}

	function clearCanvases($canvases){
		$canvases.each(function(){
			var canvasElement=$(this)[0];
			canvasElement.width=canvasElement.width;
		});
	}

	return {
		onLoad:function(){

			var source   = $("#letters-template").html();
			var template = Handlebars.compile(source);

			var letters=[];
			for(var c=0;c<lettersCount;c++){
				letters.push(c);
			}

			var $board=$(".board");

			var html=template({'letters':letters,'width':width,'height':height,'alphabet':settings.alphabet});
			$board.html(html);

			$board.find("canvas").each(function(element){
                                handleCanvasEvents($(this));
                        });

			$("#binarize").click(function(){

				$board.find("canvas").each(function(){
					Binary.binarizeCanvas($(this)[0],settings.canvasProbeSize);
				});
			});

			$("#clear").click(function(){
				$board.find("canvas").each(function(){
					var canvasElement=$(this)[0];
					canvasElement.width=canvasElement.width;
				});
			});

			$("#do").click(function(){
				Recognize.start($board.find("canvas"));
				clearCanvases($board.find("canvas"));
			});

			$("#learn").click(function(){
				NN.train();
			});

		}

	};
})(Settings);


var NN=(function(settings){
	var hiddenLayer,outputLayer;

	function displayNeurons(){
		console.log("-- hidden --")
		for(var c=0;c<hiddenLayer.length;c++){
			console.log(hiddenLayer[c]);
		}
		console.log("-- output --")
		for(var c=0;c<outputLayer.length;c++){
			console.log(outputLayer[c]);
		}
	}

	function createNeurons(){
		var lettersCount=settings.alphabet.length,
			nrOfInputs=(settings.canvasWidth/settings.canvasProbeSize)*(settings.canvasHeight/settings.canvasProbeSize);

		hiddenLayer=[];
		outputLayer=[];

		for(var c=0;c<lettersCount;c++){
			var neuron=NeuronFactory.create("hidden-neuron "+c,nrOfInputs);
			hiddenLayer.push(neuron);

			neuron=NeuronFactory.create("output-neuron ("+c+") - "+settings.alphabet[c],lettersCount);
			neuron.code=settings.alphabet[c];
			outputLayer.push(neuron);
		}
	}

	function trainNetwork(){
		Train.train(hiddenLayer,outputLayer,Dataset);
	}


	function reconcileLetter(canvasData){

		var hiddenLayerActivations=[],wonNeuron,bestActivation=0,activation;

		for(var c=0;c<hiddenLayer.length;c++){
			hiddenLayerActivations[c]=NeuronUtils.calculateActivation(hiddenLayer[c],canvasData);
		}

		for(var c=0;c<outputLayer.length;c++){
			activation=NeuronUtils.calculateActivation(outputLayer[c],hiddenLayerActivations);
			if(activation>bestActivation){
				bestActivation=activation;
				wonNeuron=outputLayer[c];
			}
		}

		displayNeurons();

		return wonNeuron;
	}


	return {
		train:function(){
			createNeurons();
			trainNetwork();
			displayNeurons();
		},
		reconcile:function(canvasData){
			return reconcileLetter(canvasData);
		}
	}
})(Settings);

var Recognize=(function(settings,nn){

	function recognize($canvases){
		var $canvas=$canvases.get()[0];
		Binary.binarizeCanvas($canvas,settings.canvasProbeSize);
		var canvasData=Binary.getMatrix($canvas,settings.canvasProbeSize);
		console.log(canvasData);
		var wonNeuron=nn.reconcile(canvasData);
		alert(wonNeuron.code);
	}

	return {
		start:function($canvases){
			recognize($canvases);
		}
	};
})(Settings,NN);

var Train=(function(settings){

	function calculateErrorForOutput(neuron,expectedValue){
		return (expectedValue - neuron.output ) * neuron.activation;
	}

	function calculateErrorForHidden(neuron,neuronNr,outputLayerNeurons){
		var learningRate=1,sumOfErrorsBetweenLayers=0,error;

		for(var n=0;n<outputLayerNeurons.length;n++){
			sumOfErrorsBetweenLayers+=outputLayerNeurons[n].error*outputLayerNeurons[n].weights[neuronNr];
		}

		return (learningRate*neuron.activation)*sumOfErrorsBetweenLayers;
	}

	function calculateWeight(neuron){
		var delta=neuron.error * neuron.output;
		for(var w=0;w<neuron.wieght;w++){
			neuron.wieghts[w]=neuron.wieghts[w]+delta;
		}
	}

	function adjustWeightsUsingErrorValue(hiddenLayerNeurons,outputLayerNeurons){
		var neuron,delta;

		// output layer
		for(var n=0;n<outputLayerNeurons.length;n++){
			calculateWeight(outputLayerNeurons[n]);
		}
		// hidden layer
		for(var n=0;n<hiddenLayerNeurons.length;n++){
			calculateWeight(hiddenLayerNeurons[n]);
		}

	}

	return {
		train:function(hiddenLayerNeurons,outputLayerNeurons,trainingSet){

			var hiddenLayerNeuronActivations,wonNeuron,expectedWonOutputValue=1,expectedLostOutputValue=0.2,errorValue;

			console.log(trainingSet);

			for(var i=0;i<trainingSet.length;i++){
				for(var c=0;c<trainingSet[i].value.length;c++){
					hiddenLayerNeuronActivations=[];

					var singleTrainingInput=trainingSet[i];
					var trainingVector=singleTrainingInput.value[c];

					// iteratre thru neurons to calculate activation

					// hidden layer

					for(var n=0;n<hiddenLayerNeurons.length;n++){
						hiddenLayerNeuronActivations[n]=NeuronUtils.calculateActivation(hiddenLayerNeurons[n],trainingVector);
					}


					// output layer

					for(var n=0;n<outputLayerNeurons.length;n++){
						NeuronUtils.calculateActivation(outputLayerNeurons[n],hiddenLayerNeuronActivations);
					}


					// calculateError

					// output layer

					for(var n=0;n<outputLayerNeurons.length;n++){

						var outpuLayerNeuron=outputLayerNeurons[n];

						if(outpuLayerNeuron.code==singleTrainingInput.letter){
							errorValue=calculateErrorForOutput(outpuLayerNeuron,expectedWonOutputValue);
						}else{
							errorValue=calculateErrorForOutput(outpuLayerNeuron,expectedLostOutputValue);
						}

						outpuLayerNeuron.error=errorValue;
					}

					// hidden layer

					for(var nh=0;nh<hiddenLayerNeurons.length;nh++){
						var hiddenNeuron=hiddenLayerNeurons[nh];
						hiddenNeuron.error=calculateErrorForHidden(hiddenNeuron,nh,outputLayerNeurons);
					}


					// adjust weights
					adjustWeightsUsingErrorValue(hiddenLayerNeurons,outputLayerNeurons);
				}
			}
		}
	};

})(Settings);

