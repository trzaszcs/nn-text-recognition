var Dataset=[
	{letter:"A",
	 value:[

	 		[0,0,0,0,0,0,0,0,
		 	0,0,0,0,0,0,0,0,
		 	0,0,0,1,0,0,0,0,
		 	0,0,1,1,1,0,0,0,
		 	0,0,1,0,1,0,0,0,
		 	0,1,1,1,1,1,0,0,
		 	0,1,0,0,0,1,0,0,
		 	1,0,0,0,0,0,1,0],

		 	[0,0,0,0,0,0,0,0,
		 	0,0,0,0,0,0,0,0,
		 	0,0,0,0,1,0,0,0,
		 	0,0,0,1,1,1,0,0,
		 	0,0,0,1,0,1,0,0,
		 	0,0,1,1,1,1,1,0,
		 	0,0,1,0,0,0,1,0,
		 	0,1,0,0,0,0,0,1],

		 	[0,0,0,0,0,0,0,0,
		 	0,0,0,0,1,0,0,0,
		 	0,0,0,1,1,0,0,0,
		 	0,0,0,1,1,1,0,0,
		 	0,0,1,1,0,1,0,0,
		 	0,0,1,1,1,1,1,0,
		 	0,0,1,0,0,0,1,0,
		 	0,1,0,0,0,0,0,1],

		 	[0,0,0,1,0,0,0,0,
		 	0,0,0,1,1,0,0,0,
		 	0,0,1,0,0,1,0,0,
		 	0,1,1,1,1,1,1,0,
		 	1,0,0,0,0,0,0,1,
		 	0,0,0,0,0,0,0,0,
		 	0,0,0,0,0,0,0,0,
		 	0,0,0,0,0,0,0,1]

		 ]
	},
	{letter:"B",
	 value:[

	 		[0,0,0,0,0,0,0,0,
		 	0,1,1,1,1,0,0,0,
		 	0,1,0,0,1,0,0,0,
		 	0,1,0,0,1,0,0,0,
		 	0,1,1,1,1,0,0,0,
		 	0,1,0,0,0,1,0,0,
		 	0,1,1,1,1,1,0,0,
		 	0,0,0,0,0,0,0,0],

		 	[0,0,0,0,0,0,0,0,
		 	0,0,0,0,0,0,1,0,
		 	0,1,1,1,1,1,1,0,
		 	0,1,1,1,0,1,1,0,
		 	0,1,1,1,1,1,1,0,
		 	0,1,1,1,1,1,0,0,
		 	0,0,0,0,1,1,0,0,
		 	0,0,0,0,0,0,0,0],

		 	[0,0,0,0,0,0,0,0,
		 	0,1,1,1,1,0,0,0,
		 	0,1,0,0,1,0,0,0,
		 	0,1,0,0,1,0,0,0,
		 	0,1,1,1,1,0,0,0,
		 	0,1,0,0,0,1,0,0,
		 	0,1,0,0,0,1,0,0,
		 	0,1,1,1,1,1,0,0]

		 ]
	},

	{letter:"C",
	 value:[

	 		[0,0,0,0,0,0,0,0,
		 	0,1,1,1,1,1,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,1,1,1,1,0,0,
		 	0,0,0,0,0,0,0,0],

		 	[0,0,0,0,0,0,0,0,
		 	0,1,1,1,1,1,1,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,1,1,1,1,1,0,
		 	0,0,0,0,0,0,0,0],

		 	[0,0,0,0,0,0,0,0,
		 	0,1,1,1,1,1,1,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,1,1,1,1,1,0],

		 	[0,1,1,1,1,1,1,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,1,1,1,1,1,0],

		 	[0,0,1,1,1,0,0,0,
		 	1,1,1,1,1,1,1,0,
		 	1,1,0,0,0,0,0,0,
		 	1,1,0,0,0,0,0,0,
		 	1,1,0,0,0,0,0,0,
		 	1,1,0,0,0,0,0,0,
		 	1,1,1,1,1,1,1,0,
		 	0,0,1,1,1,0,0,0]

		 ]
	},

	{letter:"I",
	 value:[

	 		[0,0,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,1,0,0,0,0,0,0,
		 	0,0,0,0,0,0,0,0],

		 	[0,0,0,0,0,0,0,0,
		 	0,0,1,0,0,0,0,0,
		 	0,0,1,0,0,0,0,0,
		 	0,0,1,0,0,0,0,0,
		 	0,0,1,0,0,0,0,0,
		 	0,0,1,0,0,0,0,0,
		 	0,0,1,0,0,0,0,0,
		 	0,0,0,0,0,0,0,0],

		 	[0,0,0,0,0,0,0,0,
		 	0,0,0,1,0,0,0,0,
		 	0,0,0,1,0,0,0,0,
		 	0,0,0,1,0,0,0,0,
		 	0,0,0,1,0,0,0,0,
		 	0,0,0,1,0,0,0,0,
		 	0,0,0,1,0,0,0,0,
		 	0,0,0,0,0,0,0,0],

		 	[0,0,0,0,0,0,0,0,
		 	0,0,0,0,1,0,0,0,
		 	0,0,0,0,1,0,0,0,
		 	0,0,0,0,1,0,0,0,
		 	0,0,0,0,1,0,0,0,
		 	0,0,0,0,1,0,0,0,
		 	0,0,0,0,1,0,0,0,
		 	0,0,0,0,0,0,0,0]
		 ]
	}
];

